# degov-project-skeleton
A simple project skeleton for deGov.
This composer project enables you to create your own deGov instance.

## Get Started
Use the composer-create command to setup your project. It contains a docroot folder with all deGov features. Please make sure that
that your set your document root to the docroot folder.
```
composer create-project degov/degov-project
```

## DDEV Support
We provide ready to use [ddev](https://ddev.readthedocs.io/en/stable/) configurations.

### ddev command kickstart
With the command `ddev kickstart` you start at the same starting point as our CI pipeline. So you can skip a manually installation.


## Handy Robo build tool scripts commands
The [Robo](https://robo.li/) build tool contains a few quick commands which are worth checking. E.g.:

* Quickly create an new project branch (from the develop branch via the Git flow branching model):
```
./bin/robo project:new-issue my-new-feature-branch-name
```

* Contribute into deGov and create a feature branch (from the latest release dev-branch):
```
./bin/robo degov:new-issue my-new-feature-branch-name
```

* Update deGov quickly:
```
./bin/robo degov:update
```