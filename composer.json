{
  "name": "degov/degov-project",
  "description": "Drupal 8 for Government (Drupal 8 für die öffentliche Verwaltung)",
  "type": "project",
  "license": "GPL-3.0-or-later",
  "minimum-stability": "dev",
  "prefer-stable": true,
  "require": {
    "degov/degov": "~8.4.0",
    "php": ">=7.2"
  },
  "require-dev": {
    "behat/behat": "^3.0",
    "behat/mink": "~1.7.1",
    "behat/mink-goutte-driver": "~1.2",
    "composer/composer": "^1.3",
    "consolidation/robo": "^1.4.6",
    "dealerdirect/phpcodesniffer-composer-installer": "^0.7.0",
    "degov/degov_devel_git_lfs": "~8.4.0",
    "drupal/coder": "8.*",
    "drupal/config_inspector": "^1.1",
    "drupal/console": "^1.8",
    "drupal/devel": "^4.0",
    "drupal/drupal-extension": "^4.0",
    "drupal/error_log": "^1.2",
    "drupal/stage_file_proxy": "^1.0@alpha",
    "drupal/twig_xdebug": "^1.0",
    "drush/drush": "^9.3",
    "jangregor/phpstan-prophecy": "^0.6.2",
    "jcalderonzumba/gastonjs": "~1.0.2",
    "mglaman/phpstan-drupal": "^0.12.2",
    "mikey179/vfsstream": "^1.6",
    "phpcompatibility/php-compatibility": "^9.3",
    "phpdocumentor/reflection-docblock": "^3.0.0",
    "phpstan/extension-installer": "^1.0",
    "phpstan/phpstan-deprecation-rules": "^0.12.2",
    "phpstan/phpstan-phpunit": "0.12.8",
    "phpunit/phpunit": "^7",
    "publicplan/phpcs-sniffs": "^1.0",
    "symfony/css-selector": "~2.8"
  },
  "repositories": {
    "drupal": {
      "type": "composer",
      "url": "https://packages.drupal.org/8"
    },
    "degov_devel_git_lfs": {
      "type": "git",
      "url": "https://bitbucket.org/publicplan/degov_devel_git_lfs.git"
    },
    "dropzone": {
      "type": "package",
      "package": {
        "name": "enyo/dropzone",
        "version": "4.2.0",
        "type": "drupal-library",
        "source": {
          "url": "https://github.com/enyo/dropzone",
          "type": "git",
          "reference": "origin/master"
        }
      }
    },
    "shariff": {
      "type": "package",
      "package": {
        "name": "heiseonline/shariff",
        "version": "2.1.3",
        "type": "drupal-library",
        "dist": {
          "url": "https://github.com/heiseonline/shariff/releases/download/2.1.3/shariff-2.1.3.zip",
          "type": "zip"
        },
        "require": {
          "composer/installers": "^1.7.0"
        }
      }
    },
    "slick": {
      "type": "package",
      "package": {
        "name": "kenwheeler/slick",
        "version": "1.8.1+11",
        "type": "drupal-library",
        "source": {
          "type": "git",
          "url": "https://github.com/kenwheeler/slick",
          "reference": "9c55b531245e8c866324652a353bead23f9721a0"
        },
        "require": {
          "composer/installers": "^1.7.0"
        }
      }
    },
    "leaflet": {
      "type": "package",
      "package": {
        "name": "leaflet/leaflet",
        "version": "1.5.1",
        "type": "drupal-library",
        "dist": {
          "type": "zip",
          "url": "https://github.com/Leaflet/Leaflet/archive/v1.5.1.zip"
        },
        "require": {
          "composer/installers": "^1.7.0"
        }
      }
    },
    "cropper": {
      "type": "package",
      "package": {
        "name": "fengyuanchen/cropper",
        "version": "3.1.3",
        "type": "drupal-library",
        "dist": {
          "type": "zip",
          "url": "https://github.com/fengyuanchen/cropper/archive/v3.1.3.zip"
        },
        "require": {
          "composer/installers": "^1.7.0"
        }
      }
    },
    "phpcs-sniffs-library": {
      "type": "git",
      "url": "https://bitbucket.org/publicplan/phpcs-sniffs.git"
    }
  },
  "scripts": {
    "php:cs": "phpcs -p -s",
    "php:cs-fix": "phpcbf",
    "php:compatibility": "phpcs -p -s --standard=phpcompatibility.xml",
    "php:phpstan": "phpstan analyse --ansi"
  },
  "config": {
    "bin-dir": "bin/",
    "sort-packages": true,
    "cache-dir": "~/.config/composer/cache"
  },
  "extra": {
      "drupal-scaffold": {
          "locations": {
              "web-root": "docroot/"
          },
          "file-mapping": {
              "[web-root]/example.gitignore": false,
              "[web-root]/INSTALL.txt": false,
              "[web-root]/README.txt": false,
              "[web-root]/.gitignore": false,
              "[web-root]/sites/README.txt": false,
              "[web-root]/modules/README.txt": false,
              "[web-root]/profiles/README.txt": false,
              "[web-root]/themes/README.txt": false
          }
      },
    "installer-types": ["library", "component"],
    "installer-paths": {
      "docroot/libraries/dropzone": ["enyo/dropzone"],
      "docroot/profiles/contrib/degov/testing/lfs_data": ["degov/degov_devel_git_lfs"],
      "docroot/core": ["type:drupal-core"],
      "docroot/modules/contrib/{$name}": ["type:drupal-module"],
      "docroot/profiles/contrib/{$name}": ["type:drupal-profile"],
      "docroot/themes/contrib/{$name}": ["type:drupal-theme"],
      "drush/contrib/{$name}": ["type:drupal-drush"],
      "docroot/libraries/{$name}": ["type:drupal-library"]
    },
    "enable-patching": true,
    "composer-exit-on-patch-failure": true
  },
  "autoload-dev": {
    "psr-4": {
      "Drupal\\degov\\Behat\\Context\\": "docroot/profiles/contrib/degov/testing/behat/context/",
      "Drupal\\degov\\Behat\\Context\\Traits\\": "docroot/profiles/contrib/degov/testing/behat/context/Traits/",
      "Drupal\\Tests\\Behat\\Context\\": "docroot/profiles/contrib/degov/testing/behat/context",
      "Drupal\\degov_behat_extension\\BehatContext\\": "docroot/profiles/contrib/degov/modules/degov_behat_extension/src/BehatContext",
      "Drupal\\Tests\\lightning_media\\": "docroot/profiles/contrib/degov/modules/lightning_media/tests/src",
      "degov\\Scripts\\Robo\\": "docroot/profiles/contrib/degov/scripts/Robo"
    },
    "classmap": [
      "docroot/profiles/contrib/degov/modules/lightning_core/tests/contexts/AwaitTrait.inc"
    ]
  }
}
